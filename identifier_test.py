from google.protobuf import empty_pb2
from identifier import IdentifierGenerator

def test_identifiers_are_6_characters_long():
  gen = IdentifierGenerator()
  identifier = gen.Generate(empty_pb2.Empty(), None)
  assert len(identifier.code) == 6

