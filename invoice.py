import grpc
import os

from google.protobuf import empty_pb2

from identifier_pb2_grpc import IdentifierGeneratorStub
from invoice_pb2 import NO_ITEMS, NO_CREATED_DATE
from invoice_pb2_grpc import InvoiceProcessorServicer, \
    add_InvoiceProcessorServicer_to_server as adder
from server import start_and_wait

class InvoiceProcessor(InvoiceProcessorServicer):
  def __init__(self, identifier):
    self.identifier = identifier

  def Process(self, invoice, context):
    if not validate(invoice):
      return invoice
    invoice.id = self.identifier.Generate(empty_pb2.Empty()).code
    invoice.processed.GetCurrentTime()
    return invoice

def validate(invoice):
  if not invoice.HasField('created'):
    invoice.errors.append(NO_CREATED_DATE)
  if len(invoice.items) == 0:
    invoice.errors.append(NO_ITEMS)
  return len(invoice.errors) == 0

if __name__ == '__main__':
  identifier_url = os.environ.get('ACME_IDENTIFIER_URL', 'localhost:50052')
  channel = grpc.insecure_channel(identifier_url)
  stub = IdentifierGeneratorStub(channel)
  start_and_wait(50051, InvoiceProcessor(stub), adder)

