from random import choices, normalvariate, randint
from google.protobuf import timestamp_pb2

from invoice_pb2 import Item, Invoice

def now():
  ts = timestamp_pb2.Timestamp()
  ts.GetCurrentTime()
  return ts

def item():
  return Item(
    sku = hex(randint(0x1111, 0xFFFF))[2:],
    price = round(normalvariate(20, 8), 2),
    quantity = choices([1,2,3,4,5], [65,15,10,5,5])[0]
  )

def invoice():
  return Invoice(
    created = now(),
    items = [item() for i in range(1, randint(2, 6))]
  )

